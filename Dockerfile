#imagen base
FROM node:latest

#Directorio de la app en el contenedor
WORKDIR /app

#Copiado de archivos
ADD . /app

#Dependencias
RUN npm install

#puerto que expongo
EXPOSE 3000

#COMANDOS de ejecucion de la aplicacion
CMD ["npm","start"]
