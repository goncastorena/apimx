var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var movimientosv2JSON= require("./movimientosv2.json");

var bodyparser = require('body');
app.use(bodyparser)
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req,res){

  res.sendFile(path.join(__dirname,'index.html'));
})

app.post('/',function (req,res){
  res.send("Hemos recibido su peticion post");
})

app.put('/',function (req,res){
  res.send("Hemos recibido su peticion put");
})

app.delete('/',function (req,res){
  res.send("Hemos recibido su peticion delete con api");
})


app.get('/Clientes/:idcliente',function(req,res){

  res.send(path.join('Aqui tiene al cliente numero: '+ req.params.idcliente));
})

app.get('/V1/movimientos',function(req,res){

  res.sendfile(path.join('movimientosv1.json'));
})

app.get('/V2/movimientos',function(req,res){
  res.json(path.join(movimientosv2JSON))
})

app.get('/V2/movimientos:id',function(req,res){
  console.log(req.params.id);
  res.send(movimientosv2JSON[req.params.id]);

})

app.post('/V2/movimientos', function(req,res){
  var nuevo= req.body
  nuevo.id=movimientosv2JSON.lenght+1;
  movimientosv2JSON.push(nuevo);
  res.send('movimiento dado de alta con id: '+ nuevo.id);
})
